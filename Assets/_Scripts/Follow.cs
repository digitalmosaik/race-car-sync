using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public float smoothSpeed = 0.125f;
    public Transform target;
    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.position, smoothSpeed);
        transform.rotation = target.rotation;
    }
}
