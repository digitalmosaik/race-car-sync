﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerControl.cs" company="Exit Games GmbH">
//   Part of: Photon Unity Networking Demos
// </copyright>
// <summary>
//  Used in SlotRacer Demo
// </summary>
// <author>developer@exitgames.com</author>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.Demo.SlotRacer.Utils;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;

/// <summary>
/// Player control. 
/// Interface the User Inputs and PUN
/// Handle the Car instance 
/// </summary>
public class PlayerInstantiator : MonoBehaviour, IMatchmakingCallbacks
{
    public GameObject mainCamera;
    public GameObject carPrefab;

    public int index = 0;
    
    public virtual void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public virtual void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void OnFriendListUpdate(List<FriendInfo> friendList)
    {
        Debug.Log("OnFriendListUpdate");
    }
    public void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
    }
    public void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnCreateRoomFailed");
    }
    public void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        GameObject car = PhotonNetwork.Instantiate(carPrefab.name,Vector3.zero, Quaternion.identity);
    }
    public void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRoomFailed");
    }
    public void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRandomFailed");

    }
    public void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
    }
}