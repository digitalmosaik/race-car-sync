using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ShowInfo : MonoBehaviour
{
    public static ShowInfo Instance;

    /// <summary>
    /// The grid positions.
    /// </summary>
    public TextMeshProUGUI text;

    void Awake()
    {
        text = gameObject.GetComponent<TextMeshProUGUI>();
        Instance = this;
    }
}
