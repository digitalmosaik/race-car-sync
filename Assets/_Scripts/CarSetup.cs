using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityEngine;

public class CarSetup : MonoBehaviourPun
{
    public GameObject car;
    public GameObject carCamera;

    private IEnumerator Start()
    {
        car.SetActive(false);
        // Wait until a Player Number is assigned
        // PlayerNumbering component must be in the scene.
        yield return new WaitUntil(() => this.photonView.Owner.GetPlayerNumber() >= 0);

        // now we can set it up.
        this.SetupCarOnTrack(this.photonView.Owner.GetPlayerNumber());
        yield return new WaitForSeconds(0.5f);
        if (Camera.main != null)
        {
            Destroy(Camera.main.gameObject);
        }
        if (!photonView.IsMine)
        {
            Destroy(carCamera);
        }
        car.SetActive(true);
    }

    private void SetupCarOnTrack(int index)
    {
        Transform currentTransform = SlotLanes.Instance.gridTransforms[index];
        car.transform.position = currentTransform.position;
        car.transform.rotation = currentTransform.rotation;
    }
}