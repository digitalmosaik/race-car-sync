using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMover : MonoBehaviourPun
{
    public Camera playerCamera;
    public float speed = 5f;
    public float rotationSpeed = 1f;

    private CharacterController _characterController;

    private float _inputX;
    private float _inputY;
    private void Awake()
    {
        _characterController = gameObject.GetComponent<CharacterController>();
    }
    private void Start()
    {
        playerCamera.gameObject.SetActive(photonView.IsMine);
        if (!photonView.IsMine)
        {
            Destroy(_characterController);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!photonView.IsMine) return;
        
        _inputY = Input.GetAxis("Vertical");
        _inputX = Input.GetAxis("Horizontal");
        
        _characterController.Move(transform.forward * speed * _inputY * Time.deltaTime);
        transform.Rotate(Vector3.up * rotationSpeed * _inputX * Time.deltaTime);
    }
}
