using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Car : MonoBehaviourPun, IPunObservable
{
    public float speed = 900f;
    public float rotationSpeed = 100f;
    
    private Rigidbody _rigidbody;
    private float _inputX;
    private float _inputY;
    private Vector3 _netPosition;
    private Quaternion _netRotation;
    private Vector3 _previousPos;
    
    private void Awake()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_rigidbody.position);
            stream.SendNext(_rigidbody.rotation);
            stream.SendNext(_rigidbody.velocity);
        }
        else
        {
            _netPosition = (Vector3)stream.ReceiveNext();
            _netRotation = (Quaternion)stream.ReceiveNext();
            _rigidbody.velocity = (Vector3)stream.ReceiveNext();
            
            float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.SentServerTime));
            _netPosition += (_rigidbody.velocity * lag);
            ShowInfo.Instance.text.text = lag.ToString();
        }
    }
    
    void FixedUpdate()
    {
        if (photonView.IsMine)
        {
            _inputY = Input.GetAxis("Vertical");
            _inputX = Input.GetAxis("Horizontal");

            _rigidbody.AddForce(transform.forward * speed * _inputY * Time.deltaTime, ForceMode.Acceleration);
            transform.Rotate(Vector3.up * rotationSpeed * _inputX * Time.deltaTime);
        }
        else
        {
            _rigidbody.position = Vector3.MoveTowards(_rigidbody.position, _netPosition, Time.fixedDeltaTime);
            _rigidbody.rotation = Quaternion.RotateTowards(_rigidbody.rotation, _netRotation, Time.fixedDeltaTime * 100.0f);
        }
    }
}
